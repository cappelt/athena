ApplicationMgr.OutputLevel = 5;
ApplicationMgr.DLLs += { "InDetPhysValMonitoring" };
ApplicationMgr.CreateSvc += { "HistogramDefinitionSvc/HistogramDefinitionSvc" };
HistogramDefinitionSvc.DefinitionFormat = "text/xml";
HistogramDefinitionSvc.DefinitionSource = "entityMaster.xml";
MessageSvc.OutputLevel = 5;
